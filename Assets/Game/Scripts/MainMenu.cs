﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour {

	public void TapToPlay()
    {
        SceneManager.LoadScene(1);
    }
}
