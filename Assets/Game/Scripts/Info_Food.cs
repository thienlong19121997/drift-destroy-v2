﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Info_Food : MonoBehaviour {

    public Text text0, text1,textPrice;
    public int damageHead, damageDrift;
    public int price;

    public void Start()
    {
        text0.text = "HIT : " + damageHead + " floor ruined";
        text1.text = "Drift : " + damageDrift + " floors ruined";
        textPrice.text = price.ToString();
    }
}
