﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckGrounded : MonoBehaviour {

   public  float distanceToGround = 2;
   
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        Debug.Log(IsGrounded());
	}

    public bool IsGrounded()
    {
       return Physics.Raycast(transform.position, -Vector3.up, distanceToGround + 0.1f , Car.instance.layerGround);
    }

    
}
