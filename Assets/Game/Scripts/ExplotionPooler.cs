﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExplotionPooler : MonoBehaviour {


    public static ExplotionPooler instance;

    public GameObject prefab;
    private List<GameObject> listObjects = new List<GameObject>();
    private const int count = 10;

    private void Awake()
    {
        instance = this;
    }


    void Start()
    {
        for (int i = 0; i < count; i++)
        {
            GenerateObject();
        }
    }

    private void GenerateObject()
    {
        GameObject objectClone = Instantiate(prefab, transform) as GameObject;
        objectClone.transform.position = Vector3.zero;
        objectClone.SetActive(false);
        listObjects.Add(objectClone);
    }

    public GameObject GetObject()
    {
        for (int i = 0; i < listObjects.Count; i++)
        {
            if (listObjects[i].activeInHierarchy == false)
            {
                return listObjects[i];
            }
        }

        GameObject objectClone = Instantiate(prefab, transform) as GameObject;
        objectClone.transform.position = Vector3.zero;
        objectClone.SetActive(false);
        listObjects.Add(objectClone);
        return objectClone;
    }



    public class Object
    {
        public GameObject prefab;
        private List<GameObject> listObjects = new List<GameObject>();
        private const int count = 10;
    }
}
