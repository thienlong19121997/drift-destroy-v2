﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SetColor : MonoBehaviour {

    private Color a, b;
    public bool isTamGiac;
    private List<GameObject> objectList = new List<GameObject>();
    private List<Renderer> rendList = new List<Renderer>();
    private List<float> tList = new List<float>();
    float currentT;
    float t;
    int r;
    public int count;
    public int number;
    public GameObject canvasNumber;

	public void Start () {


        CylinderManager.instance.listSetColor.Add(gameObject.GetComponent<SetColor>());

        count = CylinderManager.instance.listColorCylinder.Count;
        number = transform.childCount  ;
        r = Random.Range(0, count);
  

        for (int i = 0; i < transform.childCount; i++)
        {
            objectList.Add(transform.GetChild(i).gameObject);
            rendList.Add(objectList[i].GetComponent<Renderer>());
        }

        currentT = 1 / (float)objectList.Count;

        for(int i = 0; i < objectList.Count; i++)
        {
            t += currentT;
            tList.Add(t);
        }

        ChangeColorToNormal();

        SpawnCanvasNumber();

        // fix pos z truc tam giac

        if (isTamGiac == false)
            return;

        for(int i = 0; i < transform.childCount - 1; i++)
        {
            transform.GetChild(i).gameObject.transform.localPosition -= new Vector3(0, 0, 3);
        }

    }

    public void ChangeColorToFever()
    {
        a = CylinderManager.instance.listColorCylinderFever[r].from;
        b = CylinderManager.instance.listColorCylinderFever[r].to;
        deltaT = 0;
        for (int i = 0; i < objectList.Count; i++)
        {
            rendList[i].material.color = Color.Lerp(a, b, tList[i]);

        }
    }

    public void ChangeColorToNormal()
    {
        a = CylinderManager.instance.listColorCylinder[r].from;
        b = CylinderManager.instance.listColorCylinder[r].to;
        deltaT = 0;

        for (int i = 0; i < objectList.Count; i++)
        {
         rendList[i].material.color = Color.Lerp(a, b, tList[i]);
            
        }
    }

    float deltaT;

    private void Update()
    {
        //if (Input.GetKeyDown(KeyCode.N))
        //{
        //    ChangeColorToNormal();
        //}else if (Input.GetKeyDown(KeyCode.F))
        //{
        //    ChangeColorToFever();
        //}

        //if(deltaT < 1)
        //{
        //    deltaT += Time.deltaTime*2;
        //}
        //else
        //{
        //    return;
        //}

        // for(int i = 0; i < objectList.Count; i++)
        //{
        //    if(deltaT < tList[i])
        //    {
        //        rendList[i].material.color = Color.Lerp(a, b, deltaT);
        //    }
        //    else
        //    {
        //        rendList[i].material.color = Color.Lerp(a, b, tList[i]);
        //    }
        //}
    }

    private Text numberTExt;

    private void SpawnCanvasNumber()
    {
        GameObject canvasNumberObject = Instantiate(canvasNumber, transform) as GameObject;
        numberTExt = canvasNumberObject.transform.GetChild(0).gameObject.GetComponent<Text>();
        numberTExt.text = number.ToString();
    }

    public void UpdateNumber()
    {
        number--;
        numberTExt.text = number.ToString();
        if (number <= 0 )
        {
            numberTExt.enabled = false;
        }
    }

}
