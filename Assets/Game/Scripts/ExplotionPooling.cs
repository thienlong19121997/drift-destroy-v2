﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExplotionPooling : MonoBehaviour {

    public static ExplotionPooling instance;

    [System.Serializable]
    public class Object
    {
        public GameObject prefab;
        public int cout;
        public List<GameObject> listObject = new List<GameObject>();
    }

  
    public List<Object> object_type = new List<Object>();

    private void Awake()
    {
        instance = this;
    }

    void Start () {
        SpawnObjectPooling();
    }
	
	private void SpawnObjectPooling()
    {
        for(int i = 0; i < object_type.Count; i++)
        {

            for(int j = 0; j < object_type[i].cout; j++)
            {
                GameObject objectClone = Instantiate(object_type[i].prefab, transform) as GameObject;
                objectClone.SetActive(false);
                object_type[i].listObject.Add(objectClone);
            }
    
        }
    }

    public GameObject GetObject(int number)
    {
        for(int i = 0; i < object_type[number].listObject.Count; i++)
        {
            {
            if(object_type[number].listObject[i].activeInHierarchy == false)
                return object_type[number].listObject[i];
            }
        }

        GameObject objectClone = Instantiate(object_type[number].prefab, transform) as GameObject;
        objectClone.SetActive(false);
        object_type[number].listObject.Add(objectClone);
        return objectClone;
    }
}
