﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Helicopter : MonoBehaviour {

    public GameObject target;
    public float speed;
    public float rotateSpeed;
    Rigidbody rig;

    public float distance;
	void Start () {
        rig = GetComponent<Rigidbody>();
	}
	
	void Update () {
        //  transform.Translate(0, 0, speed * Time.deltaTime);

        if (timeCd > 0)
            timeCd -= Time.deltaTime;

        Vector2 player = new Vector2(target.transform.position.x, target.transform.position.z);
        Vector2 helicopter = new Vector2(transform.position.x, transform.position.z);
        distance = Vector2.Distance(player, helicopter);
        if(distance < 10)
        {
            if (timeCd > 0)
                return;

            timeCd = 10;
            Shoot();            
        }
    }

    private void FixedUpdate()
    {
        rig.velocity = transform.forward * speed ;


        Vector2 player = new Vector2(target.transform.position.x, target.transform.position.z);
        Vector2 helicopter = new Vector2(transform.position.x, transform.position.z);
        Vector2 dir = player - helicopter;
        float an = Mathf.Atan2(dir.x, dir.y) * Mathf.Rad2Deg;
        var newRotation = Quaternion.Euler(0, an, 0);
        transform.rotation = Quaternion.Lerp(transform.rotation, newRotation, rotateSpeed * Time.deltaTime);



    }

    float timeCd;



    private void Shoot()
    {
        float x = Random.Range(target.transform.position.x - 30, target.transform.position.x + 30);
        float z = Random.Range(target.transform.position.z - 30, target.transform.position.z + 30);

        GameObject areaBomb = AreaBombPooling.instance.GetObject();
        areaBomb.transform.position = new Vector3(x, 0, z);
        areaBomb.SetActive(true);
    }
}
