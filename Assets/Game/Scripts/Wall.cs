﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Wall : MonoBehaviour {

    public bool isFade;
    private Renderer rend;
    float a = 1;
    public BoxCollider boxCollider;
    public MeshCollider meshCollider;
    // Use this for initialization
    void Start()
    {
        rend = GetComponent<Renderer>();
        boxCollider = GetComponent<BoxCollider>();
        meshCollider = GetComponent<MeshCollider>();
    }

	
	// Update is called once per frame
	void Update () {
        if (isFade)
        {
            if(boxCollider != null)
            {
                boxCollider.enabled = false;
            }

            if(meshCollider != null)
            {
                meshCollider.enabled = false;
            }

            if (a < 0)
            {
                gameObject.SetActive(false);
                return;

            }

            a -= Time.deltaTime*2;
            Color color = rend.material.color;
            color.a = a;
            rend.material.color = color;
        }
	}
}
