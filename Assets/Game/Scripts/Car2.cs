﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.AI;
using UnityEngine.UI;

public class Car2 : MonoBehaviour {


    public static Car2 instance;

    [Header("Info")]
    public int myLevel;
    public float distanceTarget;
    public float myDistance;
    public bool isStartGame;
    public bool isGameOver;


    [Header("Physic")]
    private Rigidbody rig;
    public float moveSpeed;

    [Header("GameObject")]
    public GameObject targetDirection;
    public GameObject cameraMain;
    public GameObject taptoplayObject;
    public GameObject gameOverMenu;
    public GameObject completeMenu;
    // distance value manager

    public Image barDistance;
    public Text myDistanceText;
    public Text targetDistanceText;
    public Text myLevelText;
    public Text targetLevelText;
    Vector3 a_position_distance, b_position_distance;
    float c_magnitute_ab;


    private void Awake()
    {
        instance = this;
        rig = GetComponent<Rigidbody>();
    }

    private void Start()
    {
        InitGame();
        //  StartGame();

    }

    private void FixedUpdate()
    {
        Vector3 direction = targetDirection.transform.position - transform.position;
        rig.velocity = direction.normalized * moveSpeed;

        if (isStartGame == false || isGameOver)
            return;


        DistanceManage();
       // RiftControl();
       // CarController();


    }

    private void Update()
    {
        RiftControl();
    }


    private void LateUpdate()
    {
        CameraMainController();
    }

    private void InitGame()
    {
        myLevel = PlayerPrefs.GetInt("mylevel");

        if (myLevel >= LevelManager.instance.levels.Count)
        {
            myLevel = Random.Range(0, LevelManager.instance.levels.Count);
        }
        distanceTarget = LevelManager.instance.levels[myLevel].distance;

        transform.position = LevelManager.instance.levels[myLevel].startPosition.transform.position;

        Vector3 temp = transform.eulerAngles;
        temp.y = LevelManager.instance.levels[myLevel].angle;
        transform.eulerAngles = temp;

        Vector3 targetPositon = new Vector3(transform.position.x, cameraMain.transform.position.y, transform.position.z);
        cameraMain.transform.position = targetPositon;

        for (int i = 0; i < LevelManager.instance.levels[myLevel].startPositionEnemy.Count; i++)
        {
            GameObject enemy = EnemyPooler.instance.GetObject();

            Vector3 tempe = enemy.transform.eulerAngles;
            tempe.y = transform.eulerAngles.y;
            enemy.transform.position = tempe;

            enemy.SetActive(true);
            enemy.transform.position = LevelManager.instance.levels[myLevel].startPositionEnemy[i].transform.position;

            NavMeshAgent nav = enemy.GetComponent<NavMeshAgent>();
            nav.enabled = true;

        }

        isStartGame = false;
        isGameOver = false;

        myDistance = 0;
        a_position_distance = b_position_distance = transform.position;
        c_magnitute_ab = 0;
        myDistanceText.text = "0";
        targetDistanceText.text = distanceTarget + "";

        barDistance.fillAmount = 0;

        myLevelText.text = "" + (PlayerPrefs.GetInt("mylevel") + 1);
        targetLevelText.text = "" + (PlayerPrefs.GetInt("mylevel") + 2);
    }

    public void StartGame()
    {
        taptoplayObject.SetActive(false);
        isStartGame = true;

    }


    private void DistanceManage()
    {
        a_position_distance = transform.position;
        c_magnitute_ab = (a_position_distance - b_position_distance).magnitude;
        b_position_distance = transform.position;

        myDistance += c_magnitute_ab * .5f;
        myDistanceText.text = (int)myDistance + "";
        barDistance.fillAmount = myDistance / (float)(distanceTarget);

        if ((int)myDistance >= distanceTarget)
        {
            Debug.Log("complete level");
            Complete();
        }
    }

    private void Complete()
    {
        isGameOver = true;
        int lvl = PlayerPrefs.GetInt("mylevel");
        lvl++;
        PlayerPrefs.SetInt("mylevel", lvl);

        completeMenu.SetActive(true);
    }

    private void CarController()
    {
        Vector3 direction = targetDirection.transform.position - transform.position;
        rig.velocity = direction.normalized * moveSpeed;

    }




    public void RiftControl()
    {
        OnTapDownLeft();
        OnTapDownRight();

        OnTapUpLeft();
        OnTapUpRight();

    }

    public float speedRotate;
    public bool isTapLeft;
    public bool isTapRight;


    public void OnTapDownLeft()
    {
        if (Input.GetKey(KeyCode.A))
        {
            if (isTapRight)
                return;

            isTapLeft = true;

            transform.Rotate(0, -1 * speedRotate * Time.deltaTime, 0);
        }
    }

    public void OnTapUpLeft()
    {

        if (Input.GetKeyUp(KeyCode.A))
        {
            isTapLeft = false;
        }
    }

    public void OnTapDownRight()
    {

        if (Input.GetKey(KeyCode.D))
        {
            Debug.Log(" down right ");
            if (isTapLeft)
                return;

            isTapRight = true;

            transform.Rotate(0, 1 * speedRotate * Time.deltaTime, 0);
        }
    }

    public void OnTapUpRight()
    {


        if (Input.GetKeyUp(KeyCode.D))
        {
            isTapRight = false;
        }
    }

    private void CameraMainController()
    {
        Vector3 targetPositon = new Vector3(transform.position.x, cameraMain.transform.position.y, transform.position.z);
        cameraMain.transform.position = Vector3.Lerp(cameraMain.transform.position, targetPositon, 1 * Time.deltaTime);

        //Vector3 targetAngle = new Vector3(cameraMain.transform.eulerAngles.x, transform.eulerAngles.y, cameraMain.transform.eulerAngles.z);
        //cameraMain.transform.eulerAngles = Vector3.Lerp(cameraMain.transform.eulerAngles, targetAngle, 3 * Time.deltaTime);
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Wall")
        {

            Debug.Log(" collision wall ");
        }

        if (collision.gameObject.tag == "Enemy")
        {
            Debug.Log(" collision enemy ");

            gameObject.SetActive(false);
            collision.gameObject.SetActive(false);

            GameObject explotion = ExplotionPooler.instance.GetObject();
            explotion.transform.position = gameObject.transform.position;
            explotion.SetActive(true);

            GameObject explotion2 = ExplotionPooler.instance.GetObject();
            explotion2.transform.position = collision.gameObject.transform.position;
            explotion2.SetActive(true);

            GameOver();
        }
    }

    private void GameOver()
    {
        if (isGameOver)
            return;
        Debug.Log(" gameover ");
        gameOverMenu.SetActive(true);
        isGameOver = true;
    }





    public void LoadScene()
    {
        SceneManager.LoadScene(0);
    }


    private const float speedSmartRotate = 40.0f;
    private void SmartAutoRotate()
    {

        if (transform.eulerAngles.y <= 45 || transform.eulerAngles.y > 315)
        {
            Vector3 temp = transform.eulerAngles;

            if (transform.eulerAngles.y <= 45)
            {
                if (temp.y > 2)
                {
                    temp.y -= Time.deltaTime * speedSmartRotate;
                }
                else
                {
                    temp.y = 0;
                }
            }
            else
            {
                if (temp.y < 358)
                {
                    temp.y += Time.deltaTime * speedSmartRotate;
                }
                else
                {
                    temp.y = 0;
                }
            }

            transform.eulerAngles = temp;

        }
        else if (transform.eulerAngles.y <= 315 && transform.eulerAngles.y > 225)
        {
            Vector3 temp = transform.eulerAngles;

            if (temp.y > 272)
            {
                temp.y -= Time.deltaTime * speedSmartRotate;
            }
            else if (temp.y < 268)
            {
                temp.y += Time.deltaTime * speedSmartRotate;
            }
            else
            {
                temp.y = 270;
            }

            transform.eulerAngles = temp;
        }
        else if (transform.eulerAngles.y <= 225 && transform.eulerAngles.y > 135)
        {
            Vector3 temp = transform.eulerAngles;

            if (temp.y > 182)
            {
                temp.y -= Time.deltaTime * speedSmartRotate;
            }
            else if (temp.y < 178)
            {
                temp.y += Time.deltaTime * speedSmartRotate;
            }
            else
            {
                temp.y = 180;
            }

            transform.eulerAngles = temp;
        }
        else if (transform.eulerAngles.y <= 135 && transform.eulerAngles.y > 45)
        {
            Vector3 temp = transform.eulerAngles;

            if (temp.y > 92)
            {
                temp.y -= Time.deltaTime * speedSmartRotate;
            }
            else if (temp.y < 88)
            {
                temp.y += Time.deltaTime * speedSmartRotate;
            }
            else
            {
                temp.y = 89;
            }

            transform.eulerAngles = temp;
        }
    }
}
