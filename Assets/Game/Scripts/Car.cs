﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.AI;
using UnityEngine.UI;
using UnityEngine.PostProcessing;
using MoreMountains.NiceVibrations;
using GameAnalyticsSDK;

public class Car : MonoBehaviour {

    public static Car instance;


    [Header("Info")]
    public bool isResetData;
    public int myLevel;
    public bool isStartGame;
    public bool isGameOver;
    public bool isFever;
    public int damage = 1;
    public int damageHead;
    public int live = 2;
    public int combo;
    public float timeCombo;
    public float timeFever;
    public float timeHoldDrift;
    private bool isForceY;
    private static bool isRetry;
    public bool isGrounded;
    private float minusTimeFever;

    [Header("GameObject")]
    public GameObject lightFood;
    public GameObject lightCar;
    public GameObject canvasCar;
    public Animator feverAnim;
    public LayerMask layerGround;
    public GameObject cameraMain;
    public GameObject taptoplayObject;
    public GameObject gameOverMenu;
    public GameObject completeMenu;

    public SphereCollider capCollider;
    public PhysicMaterial bouncy;
    private Rigidbody rig;
    private Vector3 offsetCamera;

    

    public ParticleSystem trail_smoke_left_normal;
    public ParticleSystem trail_smoke_left_fever;


    public GameObject light_normal;
    public GameObject light_fever;

    public GameObject wheel_L;
    public GameObject wheel_R;

    private List<GameObject> listCar = new List<GameObject>();

    [Header("UI")]
    public Text driftValueCompleteTExt;
    public float driftValue;
    public Text driftValueText;
    private float timeRunInGame;
    public Text timeRunText;
    public Text timeRunCompleteText;

    public Image leveBar;
    public Text fromLevel, toLevel;

    public Text myLevelText;
    public int score;
    public Text scoreText;
    public int myCoin;
    public Text coinText;

    public float feverValue;
    private float timeFeverActive;
    public Animator feverBarAnim;
    public Image feverBar;

    [Header(" Color Plane ")]
    public PostProcessingBehaviour postPrecessing;
    public Renderer planeRenderer;
    private Color myColorPlane;
    public Color colorPlaneFever;
    public Color[] colorPlane;   
    


    private void Awake()
    {
        
        GameAnalytics.Initialize();
        MMVibrationManager.iOSInitializeHaptics();
        Application.targetFrameRate = 60;

        if (isResetData)
        {
            PlayerPrefs.DeleteAll();
        }
        instance = this;

        rig = GetComponent<Rigidbody>();

        for(int i = 0; i < gameObject.transform.GetChild(0).transform.childCount; i++)
        {
            listCar.Add(transform.GetChild(0).GetChild(i).gameObject);
        }
    }

    private void Start () {
        int myCar = PlayerPrefs.GetInt("mycar");
        PickCar(myCar);
        InitGame();

        if (isRetry)
        {
            isRetry = false;
            StartGame();
        }
    }

    public void PickCar(int number)
    {
        for(int i = 0; i < listCar.Count; i++)
        {
            if(i == number)
            {
                listCar[i].SetActive(true);
                if(listCar[i].gameObject.tag == "Car")
                {
                    lightCar.SetActive(true);
                    lightFood.SetActive(false);
                }
                else
                {
                    lightCar.SetActive(false);
                    lightFood.SetActive(true);
                }
            }
            else
            {
                listCar[i].SetActive(false);
            }
        }
        damage = Shop.instance.listInfo[number].GetComponent<Info_Food>().damageDrift;
        damageHead = Shop.instance.listInfo[number].GetComponent<Info_Food>().damageHead;
    }

    Vector3 myRig;
    private static bool isNextLevel;
    public Text taptoplayText;

    public void PlusDriftValue(float value)
    {
        driftValue += value;
        float d = (float)(driftValue/360.0f);
        string df = d.ToString("0.00");
        driftValueText.text = "Drift " + df;
        driftValueCompleteTExt.text = "" + df;

    }

    private void Update()
    {
        

        if (isStartGame == false || isGameOver)
            return;


        timeRunInGame += Time.deltaTime;
        timeRunText.text = timeRunInGame.ToString("0.00") + "s";
        timeRunCompleteText.text = timeRunInGame.ToString("0.00") + "s";

        if (timeCombo > 0)
        {
            timeCombo -= Time.deltaTime;
        }
        else
        {
            if (combo != 0)
            {

            }
                //combo = 0;
        }

  

    


        if (timeFever > 0)
        {
            timeFever -= Time.deltaTime;
            feverBar.fillAmount = timeFever / 10.0f;
        }
        else
        {
            if (isFever)
            {
                NormalStatus();
                isFever = false;
            }
        }

        if(isFever == false)
        {
            if(minusTimeFever > 0)
            {
                minusTimeFever -= Time.deltaTime;
            }

            if (minusTimeFever < 0)
            {
                if (feverValue > 0)
                {
                    feverValue -= Time.deltaTime*.1f;
                    feverBar.fillAmount = feverValue;
                }
            }
        }

        if (feverBar.fillAmount < feverValue && isFever == false)
        {
       
            feverBar.fillAmount += Time.deltaTime * 0.5f;
            if (feverBar.fillAmount >= 1)
            {
                FeverStatus();
            }
        }

        if (timeFeverActive > 0 && isFever == false)
        {
            timeFeverActive -= Time.deltaTime;
            
            if(timeFeverActive <= 0)
            {
                feverBarAnim.SetTrigger("Hide");
            }        
        }


        if (timeSpeed < 1)
        {
            timeSpeed += Time.deltaTime;
        }

        myRig = rig.velocity;
    }

    public void RandomMaterialPlane()
    {
        myColorPlane = colorPlane[Random.Range(0, colorPlane.Length)];
        planeRenderer.material.color = myColorPlane;
    }

    private void SetPhysicMaterial()
    {
     //  capCollider.material = bouncy;
    }

    private void InitGame()
    {
        tutorial.SetActive(false);
        conffeti.SetActive(false);
        if (isNextLevel)
        {
            taptoplayText.text = "NEXT LEVEL";
        }
        else
        {
            taptoplayText.text = "TAP TO PLAY";
        }

        leveBar.fillAmount = 0;
        postPrecessing.profile.bloom.enabled = false;
        postPrecessing.profile.chromaticAberration.enabled = false;

        offsetCamera = transform.position - cameraMain.transform.position;

        PlusCoin(0);
        score = 0;
        feverValue = 0;
        feverBar.fillAmount = feverValue;
        isStartGame = isGameOver  = false;
        myLevel = PlayerPrefs.GetInt("mylevel");
        fromLevel.text = "" + (myLevel + 1);
        toLevel.text = "" + (myLevel + 2);
        myLevelText.text = "Level  " + (myLevel+1);

        Invoke("SetPhysicMaterial", 1.5f);
    }
    
IEnumerator Tutorial(){
    tutorial.SetActive(true);
    yield return new WaitForSeconds(4);
    tutorial.SetActive(false);
    
}

    public void StartGame()
    {
        if(myLevel < 5){
            StartCoroutine(Tutorial());
        }
        isNextLevel = false;
        GameAnalytics.NewProgressionEvent(GAProgressionStatus.Start, "game");
        FacebookAnalytics.Instance.LogGame_startEvent(1);

        taptoplayObject.SetActive(false);
        isStartGame = true;
    }

    private void Complete()
    {
        isNextLevel = true;
        int levelup = myLevel + 1;
        PlayerPrefs.SetInt("mylevel", levelup);
        isGameOver = true;
        completeMenu.SetActive(true);
        StartCoroutine(PlusCoinTimeComplete());

        GameAnalytics.NewProgressionEvent(GAProgressionStatus.Complete, "game", (int)driftValue);
        FacebookAnalytics.Instance.LogGame_endEvent(1);

    }
    public GameObject btnNext;
    public GameObject tutorial;
    IEnumerator PlusCoinTimeComplete()
    {
        conffeti.SetActive(true);
        yield return new WaitForSeconds(1f);

        int c = (int)timeRunInGame;
        int r;
        if(c < 30)
        {
            r = 9;
        }else if ( c < 60)
        {
            r = 6;
        }else if (c < 90)
        {
            r = 3;
        }else
        {
            r = 1;
        }
        for(int i = 0; i < r; i++)
        {
            int coin = PlayerPrefs.GetInt("coin");
            coin += 10;
            PlayerPrefs.SetInt("coin", coin);
            coinText.text = coin.ToString();
            SFXmanager.instance.Coin_SFX();
            yield return new WaitForSeconds(.1f);
        }
                yield return new WaitForSeconds(1f);

        LoadScene();
    }

    public void CameraMainController()
    {
        canvasCar.transform.position = transform.position;
        if (isGameOver)
            return;

        Vector3 targetPositon = new Vector3(transform.position.x - offsetCamera.x, cameraMain.transform.position.y, transform.position.z - offsetCamera.z);
        cameraMain.transform.position = Vector3.Lerp(cameraMain.transform.position, targetPositon, 10 * Time.deltaTime);
       

    }

    public void SetCombo()
    {
        if (timeCombo > 0)
        {
            combo++;
        }
        else
        {
            combo = 1;
        }
        timeCombo = 10;


    }

    void FeverStatus()
    {
                BossManager.instance.LightBoss(true);

        postPrecessing.profile.bloom.enabled = true;
        postPrecessing.profile.chromaticAberration.enabled = true;
    
        CylinderManager.instance.ChangeColorCylinder(true);
        Debug.Log("FEVER");
        feverAnim.SetTrigger("Fever");
        isFever = true;
        timeFever = 10.0f;

        trail_smoke_left_normal.Stop();

        trail_smoke_left_fever.Play();

        light_fever.SetActive(true);
        light_normal.SetActive(false);

        iTween.ColorTo(planeRenderer.gameObject, colorPlaneFever, 1.0f);
    }

 

    void NormalStatus()
    {
        CylinderManager.instance.ChangeColorCylinder(false);

        BossManager.instance.LightBoss(false);
        feverValue = 0;
        feverBar.fillAmount = feverValue;

        postPrecessing.profile.bloom.enabled = false;
        postPrecessing.profile.chromaticAberration.enabled = false;

        trail_smoke_left_normal.Play();

        trail_smoke_left_fever.Stop();

        light_fever.SetActive(false);
        light_normal.SetActive(true);

        iTween.ColorTo(planeRenderer.gameObject, myColorPlane, 1.0f);
    }

    public void CollisionBoss()
    {
        GameObject explotion = ExplotionPooling.instance.GetObject(0);
        explotion.transform.position = transform.position;
        explotion.SetActive(true);

        gameObject.SetActive(false);
        GameOverGame();
    }

    private void OnTriggerEnter(Collider other)
    {     
        if(other.tag == "Coin")
        {
            other.gameObject.SetActive(false);
                        SFXmanager.instance.Coin_SFX();

            PlusCoin(5);
        }

        if (other.tag == "Obtacle")
        {
            bool isGrounded = Physics.Raycast(other.transform.position, -Vector3.up, 4 + 0.1f);

            SetCombo();

             GameObject parentCollider = other.gameObject.transform.parent.gameObject;
            HitHead(parentCollider, false);
        }


        if (other.tag == "Bomb")
        {

            GameObject explotion = ExplotionPooling.instance.GetObject(0);
            explotion.transform.position = transform.position;
            explotion.SetActive(true);

            other.gameObject.SetActive(false);

            if (isFever)
                return;

            live--;
            if (live > 0)
            {
                rig.AddForce(new Vector3(0, 1000, 0));
                return;
            }
            else
            {
                rig.freezeRotation = false;
                rig.AddForce(new Vector3(Random.Range(-150,150),400, Random.Range(-150, 150)));
            }
               



            if (isGameOver)
                return;

            GameOverGame();
        }

        if(other.tag == "OutSide")
        {
            if (isGameOver)
                return;

            GameOverGame();
        }
    }

    public float timeSpeed;
public GameObject conffeti;
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Plane")
        {
            isGrounded = true;
        }

        if (collision.gameObject.tag == "Wall")
        {
            NiceVibration(1, HapticTypes.LightImpact);

            //collision.gameObject.AddComponent<Rigidbody>();
            //Rigidbody rig = collision.gameObject.GetComponent<Rigidbody>();
            //rig.mass = 5;
            //rig.AddForce(myRig * -1 * 500);
            ContactPoint cp = collision.contacts[0];
            // calculate with addition of normal vector
            // myRigidbody.velocity = oldVel + cp.normal*2.0f*oldVel.magnitude;

            // calculate with Vector3.Reflect
            timeSpeed = 0;
            rig.velocity = Vector3.zero;
            float myRigMagnitudeFixed = Mathf.Clamp(myRig.magnitude,0, 25);
            if(collision.gameObject.transform.parent.name == "Wall_Plane_Tron")
            {
                rig.AddForce(Vector3.Reflect(myRig, cp.normal).normalized * 1750);

            }
            else
            {
                // rig.AddForce(Vector3.Reflect(myRig, cp.normal).normalized * 1000);
                Vector3 dired = Vector3.zero - transform.position;
                rig.velocity = Vector3.zero;

                rig.AddForce(dired.normalized * 1400);
            }
            

            collision.gameObject.GetComponent<Wall>().isFade = true;

            //Color color = collision.gameObject.GetComponent<Renderer>().material.color;

            //for (int i = 0; i < collision.gameObject.transform.childCount; i++)
            //{
            //    EffectExplotion(3, collision.gameObject.transform.GetChild(i).gameObject.transform.position, color);
            //}
        }
    }



    public void HitHead(GameObject parent,bool isHead)
    {
        Debug.Log(" HIT OBTACLE " + isHead);
        


        if (isHead)
        {
            d = damageHead;

            if (isFever)
                d += 1;
        }
        else
        {
            d = damage;

            if (isFever)
                d *= 2;
        }

  
        
     
        StartCoroutine(StepExplosion(parent));
    }

    int forceY;
    int d;
    public float ll;
    public void UpdateLevelBar()
    {
        ll++;
        leveBar.fillAmount = ll / CylinderManager.instance.myCylinder;

    }

    IEnumerator StepExplosion(GameObject parent)
    {
        GameObject parentCollider = parent;

        forceY = parent.transform.childCount;
 

        for (int i = 0; i < parentCollider.transform.childCount - 1; i++)
        {
            if (parentCollider.transform.GetChild(i).gameObject.activeSelf)
            {

                if (isFever == false)
                {
                    if (timeFeverActive <= 0)
                    {
                        feverBarAnim.SetTrigger("Awake");
                    }
                    timeFeverActive = 5.0f;
                    minusTimeFever = 2;


                    float fv = Mathf.Lerp(10,60,(float)myLevel/50.0f);
                   feverValue += (1.0f / fv);

                
                }


                //          Debug.Log(d);
                PlusCoin(Random.Range(1, 3));

                parentCollider.transform.GetChild(i).gameObject.SetActive(false);

                Color color = parentCollider.transform.GetChild(i).gameObject.GetComponent<Renderer>().material.color;
                EffectExplotion(1, parentCollider.transform.GetChild(i).gameObject.transform.position, color);

                for (int j = (int)(parentCollider.transform.childCount/2); j >= 0; j--)
                {
                    if (parentCollider.transform.GetChild(j).gameObject.activeSelf)
                    {
                        parentCollider.transform.GetChild(j).gameObject.GetComponent<Rigidbody>().AddForce(new Vector3(0, Mathf.Lerp(1000,15000,forceY/parentCollider.transform.childCount) , 0));
                    }
                }


                parentCollider.GetComponent<SetColor>().UpdateNumber();
                forceY--;
                d--;

             //   NiceVibration(1, HapticTypes.Failure);
                MMVibrationManager.iOSTriggerHaptics(HapticTypes.HeavyImpact);
                
                Debug.Log(" destroy ");
                CylinderManager.instance.targetCylinder--;

                UpdateLevelBar();
                if (CylinderManager.instance.targetCylinder <= 0)
                {
                    Debug.Log(" complete ");
                    Invoke("Complete", 1.2f);
                }



                if (d <= 0)
                {
             //       Debug.Log("OUT");
                    i = 99;
                    yield break;
                        
                }




                yield return new WaitForSeconds(.15f);
            }
        }

    }

    public void BossComplete()
    {
        Invoke("Complete", 1.2f);

    }

    void GameOverGame()
    {
        FacebookAnalytics.Instance.LogGame_end_levelsEvent(1);

        NiceVibration(1, HapticTypes.HeavyImpact);
        Debug.Log(" gameover ");
        isGameOver = true;
        gameOverMenu.SetActive(true);
    }

    public void LoadScene()
    {
        SceneManager.LoadScene(0);
    }

    public void BtnRetry()
    {
        SceneManager.LoadScene(0);

        //isRetry = true;
        //SceneManager.LoadScene(0);

    }

    public void EffectExplotion(int size,Vector3 pos,Color color)
    {
        GameObject explotion = ExplotionPooling.instance.GetObject(size);
        explotion.transform.position = pos;

  
            //Debug.Log(" change color ");
            //ParticleSystem ps = explotion.gameObject.GetComponent<ParticleSystem>();

            //var col = ps.colorOverLifetime;
            //col.enabled = true;


            //ParticleSystem ps1 = explotion.transform.GetChild(0).gameObject.GetComponent<ParticleSystem>();

            //var col1 = ps1.colorOverLifetime;
            //col1.enabled = true;


            //Gradient grad = new Gradient();
            //grad.SetKeys(new GradientColorKey[] { new GradientColorKey(Color.white, 0.0f), new GradientColorKey(color, 0.2f) }, new GradientAlphaKey[] { new GradientAlphaKey(1.0f, 0.0f), new GradientAlphaKey(1.0f, 1.0f) });

            //col.color = grad;
            //col1.color = grad;



    


         ParticleSystem par = explotion.gameObject.GetComponent<ParticleSystem>();
         var main = par.main;
          main.startColor = color;

        for (int i = 0; i < explotion.transform.childCount; i++)
        {
            ParticleSystem parc = explotion.transform.GetChild(i).gameObject.GetComponent<ParticleSystem>();
            var mainn = parc.main;
            mainn.startColor = color;
        }

        GameObject splash = SplashPooling.instance.GetObject();
        SpriteRenderer spr = splash.GetComponent<SpriteRenderer>();
        spr.sortingOrder = SplashPooling.instance.sortingLayer;
        SplashPooling.instance.sortingLayer++;
        spr.sprite = SplashPooling.instance.SplashsSprite[Random.Range(0, SplashPooling.instance.SplashsSprite.Length)];

        splash.transform.position = new Vector3(pos.x, 0.01f, pos.z);

        spr.color = color;

        float r = Random.Range(7, 10)/10f;
        splash.transform.localScale = new Vector3(r, r, r);

        splash.SetActive(true);

        explotion.SetActive(true);

    }



    public void PlusCoin(int coin)
    {
        int c = PlayerPrefs.GetInt("coin");
        c += coin;
        PlayerPrefs.SetInt("coin", c);

        myCoin = c;
        coinText.text = "" + c;

        if (coin == 0)
            return;

        GameObject coinTextAnim = CoinTextAnimPooling.instance.GetObject();
        coinTextAnim.GetComponent<Text>().text = "+" + coin;
        coinTextAnim.SetActive(true);
    }

    IEnumerable NiceVibration(int count, HapticTypes type)
    {
        for(int i = 0; i < count; i++)
        {
            MMVibrationManager.iOSTriggerHaptics(type);
            if(count != 1)
            {
                yield return new WaitForSeconds(0.2f);

            }
            else
            {
                yield return new WaitForSeconds(0f);
            }
        }
    }
}
