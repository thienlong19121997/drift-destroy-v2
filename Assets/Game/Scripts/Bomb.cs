﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bomb : MonoBehaviour {


    float timeExplosion;
    bool isExplosion;
    public Rigidbody rig;
    public GameObject model;
    public bool isAddForce;

    private void OnEnable()
    {
        if(isAddForce)
        rig.AddForce(new Vector3(Random.Range(-20, 20), 0, Random.Range(-20, 20)));

        isExplosion = false;
        timeExplosion = 10.0f;
        model.SetActive(false);
        Invoke("DelayEnableModel", 1);
    }

    void DelayEnableModel()
    {
        model.SetActive(true);
    }

    // Update is called once per frame
    void Update () {
        

        if (isExplosion)
            return;

		if(timeExplosion > 3.0f)
        {
            timeExplosion -= Time.deltaTime;
        }
        else
        {
            isExplosion = true;

            GameObject explotion = ExplotionPooling.instance.GetObject(2);
            explotion.transform.position = transform.position;
            explotion.SetActive(true);

            gameObject.SetActive(false);

        }
	}



    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "AreaBomb" || other.tag == "OutSide")
        {
            CancelInvoke();
            BombManager.instance.timeRun = .25f;

  

            gameObject.SetActive(false);
        }
    }
}
