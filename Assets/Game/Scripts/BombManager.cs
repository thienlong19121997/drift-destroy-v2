﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BombManager : MonoBehaviour {

    public static BombManager instance;

    private void Awake()
    {
        instance = this;
    }

    public GameObject prefab;
    private List<GameObject> listObject = new List<GameObject>();
    private List<Bomb> listBombScripts = new List<Bomb>();
    private const int count = 15;

    float space;


    public float timeCooldown = 3;
    public float timeRun;

    private void Start()
    {

        int mylevel = PlayerPrefs.GetInt("mylevel");
        if(mylevel < 10)
        {
            space = 15;
            timeCooldown = 4;
        }
        else if (mylevel < 20)
        {
            space = 13;
            timeCooldown = 3.75f;
        }
        else if (mylevel < 30)
        {
            space = 11;
            timeCooldown = 3.5f;
        }
        else if (mylevel < 40)
        {
            space = 9;
            timeCooldown = 3.25f;
        }
        else
        {
            space = 6;
            timeCooldown = 3;
        }

        for (int i = 0; i < count; i++)
        {
            GenerateObject();
        }

        if (isSpawnBombIdle == false)
        {
            isSpawnBombIdle = true;
        }
    }

    private void GenerateObject()
    {
        GameObject objectClone = Instantiate(prefab, transform) as GameObject;
        objectClone.SetActive(false);
        listObject.Add(objectClone);
        listBombScripts.Add(objectClone.GetComponent<Bomb>());
    }

    public GameObject GetObject()
    {
        for (int i = 0; i < listObject.Count; i++)
        {
            if (listObject[i].activeInHierarchy == false)
            {
                return listObject[i].gameObject;
            }
        }

        GameObject objectClone = Instantiate(prefab, transform) as GameObject;
        listObject.Add(objectClone);
        listBombScripts.Add(objectClone.GetComponent<Bomb>());


        return objectClone;
    }





    private void LateUpdate()
    {
        SpawnBomb();
    }

    float posX, posZ;
    bool isSpawnBombIdle;
    private void SpawnBomb()
    {
        if (Car.instance.isStartGame == false || Car.instance.isGameOver)
            return;

        if(timeRun <= 0)
        {
            // spawn bomb
            timeRun = timeCooldown;

            GameObject bombClone = GetObject();
            Vector3 temp = bombClone.transform.position;

            Vector3 posCar = Car.instance.transform.position;


            if(Car.instance.timeHoldDrift > 1)
            {


                if (posCar.x > 0)
                {
                    posX = Random.Range(posCar.x - space/2, posCar.x - space*2);
                }
                else
                {
                    posX = Random.Range(posCar.x + space/2, posCar.x + space*2);
                }

                if (posCar.z > 0)
                {
                    posZ = Random.Range(posCar.z - space/2, posCar.z - space*2);
                }
                else
                {
                    posZ = Random.Range(posCar.z + space/2, posCar.z + space*2);
                }

            }
            else
            {
                if (posCar.x > 0)
                {
                    posX = Random.Range(posCar.x - space, posCar.x - space * 2);
                }
                else
                {
                    posX = Random.Range(posCar.x + space, posCar.x + space * 2);
                }

                if (posCar.z > 0)
                {
                    posZ = Random.Range(posCar.z - space, posCar.z - space * 2);
                }
                else
                {
                    posZ = Random.Range(posCar.z + space, posCar.z + space * 2);
                }

                //posX = Random.Range(posCar.x - 15, posCar.x + 15);
                //posZ = Random.Range(posCar.z - 15, posCar.z + 15);
            }
  
            
            temp.x = posX;
            temp.y = 50.0f;
            temp.z = posZ;
            bombClone.transform.position = temp;

            bombClone.GetComponent<Bomb>().isAddForce = true;

            bombClone.SetActive(true);
        }
        else
        {
            timeRun -= Time.deltaTime;
        }

      
    }

    public GameObject[] targetPosition;

    public IEnumerator SpawnBomb_Idle()
    {
        for(int i = 0; i < targetPosition.Length; i++)
        {
            GameObject bombClone = GetObject();
            Vector3 temp = bombClone.transform.position;

            Vector3 posCar = Car.instance.transform.position;
            temp.x = targetPosition[i].transform.position.x;
            temp.y = 10;
            temp.z = targetPosition[i].transform.position.z;
            bombClone.transform.position = temp;

            bombClone.GetComponent<Bomb>().isAddForce = false;
            bombClone.SetActive(true);
        }

        yield return new WaitForSeconds(15);
        StartCoroutine(SpawnBomb_Idle());
    }


}
