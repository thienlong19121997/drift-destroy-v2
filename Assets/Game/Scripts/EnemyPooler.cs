﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyPooler : MonoBehaviour {

    public static EnemyPooler instance;

    public GameObject prefab;
    private List<GameObject> listObjects = new List<GameObject>();
    private List<EnemyCar> enemyCars = new List<EnemyCar>();
    private const int count = 10;

    public Transform[] paths;

    private void Awake()
    {
        instance = this;
    }


    void Start () {
		for(int i = 0; i < count; i++)
        {
            GenerateObject();
        }
	}
	
    private void GenerateObject()
    {
        GameObject objectClone = Instantiate(prefab, transform) as GameObject;
        objectClone.SetActive(false);
        listObjects.Add(objectClone);
        enemyCars.Add(objectClone.GetComponent<EnemyCar>());
    }

    public GameObject GetObject()
    {
        for(int i = 0; i < listObjects.Count; i++)
        {
            if(listObjects[i].activeInHierarchy == false)
            {
                return listObjects[i];
            }
        }

        GameObject objectClone = Instantiate(prefab, transform) as GameObject;
        objectClone.SetActive(false);
        listObjects.Add(objectClone);
        enemyCars.Add(objectClone.GetComponent<EnemyCar>());
        return objectClone;
    }

    bool isSetEnable;
    private void LateUpdate()
    {
        if (Car.instance.isGameOver)
        {
            if(isSetEnable == false)
            {
                isSetEnable = true;
               
                for (int i = 0; i < listObjects.Count; i++)
                {
                    if (listObjects[i].activeSelf)
                    {
                        NavMeshAgent nav = enemyCars[i].GetComponent<NavMeshAgent>();
                        nav.stoppingDistance = 10;
                        nav.speed = 5;
                        nav.acceleration = 30;

                    }
                }
            }
        }

        if (Car.instance.isGameOver || Car.instance.isStartGame == false)
        {
            return;
        }


        for (int i = 0; i < listObjects.Count; i++)
        {
            if (listObjects[i].activeSelf)
            {
                //enemyCars[i].UpdateStep();
            }
        }
    
  
    }
}
