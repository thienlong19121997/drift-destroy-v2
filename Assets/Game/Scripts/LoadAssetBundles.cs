﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoadAssetBundles : MonoBehaviour {

    AssetBundle myLoadedAssetBundles;
    public string path;
    


    [System.Serializable]
    public class LoadObjectFromAssetBundles{
        public string nameObject;
        public GameObject parent;
    }

    public List<LoadObjectFromAssetBundles> listObject = new List<LoadObjectFromAssetBundles>();
    private static bool hasSpawn;

    void Start () {

        if (hasSpawn)
            return;

        hasSpawn = true;


        LoadAssetBundle(path);
        
        for(int i = 0; i < listObject.Count; i++)
        {
            InstantiateObject(listObject[i].nameObject,listObject[i].parent);
        }
     
    }
	
    void LoadAssetBundle(string bundleUrl)
    {
        myLoadedAssetBundles = AssetBundle.LoadFromFile(bundleUrl);
    }


    void InstantiateObject(string assetName,GameObject parent)
    {





        var prefab = myLoadedAssetBundles.LoadAsset(assetName);
        Instantiate(prefab);
    }

}
