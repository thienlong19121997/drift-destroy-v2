﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HideObject : MonoBehaviour {

    public float timeHide;

    private void OnEnable()
    {
        StartCoroutine(Hide());
    }


    IEnumerator Hide()
    {
        yield return new WaitForSeconds(timeHide);
        gameObject.SetActive(false);
    }
}
