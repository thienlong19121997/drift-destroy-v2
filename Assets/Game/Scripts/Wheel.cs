﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Wheel : MonoBehaviour {

    public GameObject leftWheel, rightWheel;

    private void OnEnable()
    {
        Car.instance.wheel_L = leftWheel;
        Car.instance.wheel_R = rightWheel;
    }
}
