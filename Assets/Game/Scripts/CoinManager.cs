﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinManager : MonoBehaviour {
    public static CoinManager instance;

    private void Awake()
    {
        instance = this;
    }

    public GameObject prefab;
    private List<GameObject> listObject = new List<GameObject>();
    private const int count = 20;

    void Start()
    {
        for (int i = 0; i < count; i++)
        {
            GenerateObject();
        }
    }

    private void GenerateObject()
    {
        GameObject objectClone = Instantiate(prefab, transform) as GameObject;
        objectClone.SetActive(false);
        listObject.Add(objectClone);
    }

    public GameObject GetObject()
    {
        for (int i = 0; i < listObject.Count; i++)
        {
            if (listObject[i].activeInHierarchy == false)
            {
                return listObject[i].gameObject;
            }
        }

        GameObject objectClone = Instantiate(prefab, transform) as GameObject;
        listObject.Add(objectClone);

        return objectClone;
    }

    float timeRun;
    float posX, posZ;
    private void Update()
    {

        if (Car.instance.isGameOver || Car.instance.isStartGame == false)
            return;

        if(timeRun < 1.5f)
        {
            timeRun += Time.deltaTime;
        }
        else
        {
            timeRun = 0;

            GameObject coinObject = GetObject();

            Vector3 temp = coinObject.transform.position;

            Vector3 posCar = Car.instance.transform.position;
            if (posCar.x > 0)
            {
                posX = Random.Range(posCar.x - 10, posCar.x -20);
            }
            else
            {
                posX = Random.Range(posCar.x + 10, posCar.x + 20);
            }

            if(posCar.z > 0)
            {
                posZ = Random.Range(posCar.z - 10, posCar.z - 20);
            }
            else
            {
                 posZ = Random.Range(posCar.z + 10, posCar.z + 20);
            }


            temp.x = posX;
            temp.y = 0.0f;
            temp.z = posZ;
            
            coinObject.transform.position = temp;


            coinObject.SetActive(true);
        }

    }
}
