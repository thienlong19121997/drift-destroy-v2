﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Plane : MonoBehaviour {

    private void OnEnable()
    {
        Car.instance.planeRenderer = gameObject.GetComponent<Renderer>();
        Car.instance.RandomMaterialPlane();
    }
}   
