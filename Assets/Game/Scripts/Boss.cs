﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Boss : MonoBehaviour {

    public GameObject light;
    public float movementSpeed;
    public float rotateSpeed;

    public bool isGrounded;

    public GameObject target;

    private void OnEnable()
    {
        isGrounded = false;
        target = Car.instance.gameObject;
        sp = 0;

        if(Car.instance.isFever){
SetLight(true);
        }else{
SetLight(false);

        }
    }

    float sp;

    bool isAwake;

public void SetLight(bool isActive){
    light.SetActive(isActive);
}
    public void UpdateStep()
    {

        if(Car.instance.isGameOver)
        return;



        if (isGrounded == false)
            return;

        if(isAwake == false)
        {
            if(sp < 1)
            {
                sp += Time.deltaTime * .75f;
            }
            else
            {
                isAwake = true;
            }
        }

        float b = Vector3.Distance(target.transform.position, transform.position);

        if (Car.instance.timeHoldDrift > 2 && b < 20)
        {
            if(sp < 2)
            {
                sp += Time.deltaTime/2;
            } 
            transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(target.transform.position - transform.position), rotateSpeed * Time.deltaTime * sp * 2);
            transform.Translate(Vector3.forward * movementSpeed * Time.deltaTime * sp);
        }
        else
        {
            if(sp > 1)
            {
                sp -= Time.deltaTime;
            }

            transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(target.transform.position - transform.position), rotateSpeed * Time.deltaTime * sp);
            transform.Translate(Vector3.forward * movementSpeed * Time.deltaTime * sp);

        }


    }


    private void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.tag == "Plane")
        {
            isGrounded = true;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "OutSide")
        {
            gameObject.SetActive(false);
        }

        if (other.tag == "Obtacle")
        {


            GameObject parentCollider = other.gameObject.transform.parent.gameObject;
            HitHead(parentCollider);
        }

        if (other.tag == "Player")
        {
            if(Car.instance.isGameOver){
                return;
            }
            // kill player and this -> explosion
            Car.instance.CollisionBoss();
            gameObject.SetActive(false);
        }

        if(other.tag == "Bomb")
        {
            GameObject explotion = ExplotionPooling.instance.GetObject(0);
            explotion.transform.position = transform.position;
            explotion.SetActive(true);

            other.gameObject.SetActive(false);
            gameObject.SetActive(false);
        }
    }

    public void HitHead(GameObject parent)
    {
        StartCoroutine(StepExplosion(parent));
    }

    int forceY;
    int d;


    IEnumerator StepExplosion(GameObject parent)
    {
        GameObject parentCollider = parent;

        forceY = parent.transform.childCount;


        for (int i = 0; i < parentCollider.transform.childCount - 1; i++)
        {
            if (parentCollider.transform.GetChild(i).gameObject.activeSelf)
            {
                parentCollider.transform.GetChild(i).gameObject.SetActive(false);

                Color color = parentCollider.transform.GetChild(i).gameObject.GetComponent<Renderer>().material.color;
                Car.instance.EffectExplotion(1, parentCollider.transform.GetChild(i).gameObject.transform.position, color);

                for (int j = (int)(parentCollider.transform.childCount/2); j >= 0; j--)
                {
                    if (parentCollider.transform.GetChild(j).gameObject.activeSelf)
                    {
                        parentCollider.transform.GetChild(j).gameObject.GetComponent<Rigidbody>().AddForce(new Vector3(0, Mathf.Lerp(1000, 5000, forceY / parentCollider.transform.childCount-1), 0));
                    }
                }

                forceY--;
                d--;
                Debug.Log(" destroy ");
                CylinderManager.instance.targetCylinder--;
                Car.instance.UpdateLevelBar();
                if (CylinderManager.instance.targetCylinder <= 0)
                {
                    Debug.Log(" complete ");
                    Car.instance.BossComplete();
                }

                if (d <= 0)
                {
                    i = 99;
                    yield break;

                }




                yield return new WaitForSeconds(.125f);
            }
        }

    }
}
