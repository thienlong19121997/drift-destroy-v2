﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Head : MonoBehaviour {

    private void OnTriggerEnter(Collider collision)
    {
        if (collision.tag == "Obtacle")
        {
            bool isGrounded = Physics.Raycast(collision.transform.position, -Vector3.up, 3 + 0.1f);

            Car.instance.SetCombo();

            Debug.Log(" hit head ");
            GameObject parentCollider = collision.gameObject.transform.parent.gameObject;

            Car.instance.HitHead(parentCollider,true);
        }
    }
}
