﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CylinderManager : MonoBehaviour {

    public static CylinderManager instance;

    public int targetCylinder;
    public int myCylinder;


    [System.Serializable]
    public class ColorCylinderNormal
    {
        public Color from, to;
    }

    public List<ColorCylinderNormal> listColorCylinder = new List<ColorCylinderNormal>();

    [System.Serializable]
    public class ColorCylinderFever
    {
        public Color from, to;
    }

    public List<ColorCylinderFever> listColorCylinderFever = new List<ColorCylinderFever>();

    public List<SetColor> listSetColor = new List<SetColor>();

    private void Awake()
    {
        instance = this;
            
    }

    private List<GameObject> levels = new List<GameObject>();

    // Use this for initialization
    void Start () {
        InitLevel();
        RandomLevel();		
	}

    private void RandomLevel()
    {
        int r = PlayerPrefs.GetInt("mylevel");
        if(r >= 49)
        {
            r = Random.Range(30, 49);
        }

        for (int i = 0; i < levels[r].transform.childCount; i++)
        {
            for(int j = 0; j < levels[r].transform.GetChild(i).transform.childCount;j++)
            {
                if(levels[r].transform.GetChild(i).transform.GetChild(j).gameObject.tag == "Obtacle")
                {
                    targetCylinder++;
                    myCylinder++;
                }  
            }
        }

        levels[r].SetActive(true);
    }

    private void InitLevel()
    {
        for(int i = 0; i < transform.childCount; i++)
        {
            levels.Add(transform.GetChild(i).gameObject);
        }
    }

    public void ChangeColorCylinder(bool isFever)
    {
        if (isFever == false)
        {
            for (int i = 0; i < listSetColor.Count; i++)
            {
                listSetColor[i].ChangeColorToNormal();
            }
        }
        else
        {
            for (int i = 0; i < listSetColor.Count; i++)
            {
                listSetColor[i].ChangeColorToFever();
            }
        }

        
    }
}
