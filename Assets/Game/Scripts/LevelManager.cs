﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelManager : MonoBehaviour {

    public static LevelManager instance;

    private void Awake()
    {
        instance = this;
    }

    [System.Serializable]
    public class Level
    {
        public float distance;

        public GameObject startPosition;
        public float angle;

        public List<GameObject> startPositionEnemy = new List<GameObject>();

    }

    //[System.Serializable]
    //public class Enemy
    //{
    //    public List<GameObject>
    //}

    public List<Level> levels = new List<Level>();

}
