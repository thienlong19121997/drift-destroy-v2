﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossManager : MonoBehaviour {

    public static BossManager instance;

    private void Awake()
    {
        instance = this;
    }

    public int levelSpawnBoss;
    public GameObject prefab;
    private List<GameObject> listObject = new List<GameObject>();
    public List<Boss> listBoss = new List<Boss>();
    private const int count = 3;
        
    public void LightBoss(bool isActive){
        for(int i = 0 ; i < listBoss.Count;i++){
            listBoss[i].SetLight(isActive);
        }
    }
    // Use this for initialization
    void Start () {
        for (int i = 0; i < count; i++)
        {
            GenerateObject();
        }
    }
	
    private void GenerateObject()
    {
        GameObject objectClone = Instantiate(prefab, transform) as GameObject;
        objectClone.SetActive(false);
        listObject.Add(objectClone);
        listBoss.Add(objectClone.GetComponent<Boss>());

    }

    public GameObject GetObject()
    {
        for (int i = 0; i < listObject.Count; i++)
        {
            if (listObject[i].activeInHierarchy == false)
            {
                return listObject[i].gameObject;
            }
        }

        GameObject objectClone = Instantiate(prefab, transform) as GameObject;
        listObject.Add(objectClone);
        listBoss.Add(objectClone.GetComponent<Boss>());

        return objectClone;
    }

    float timeGame;
    bool isSpawnBoss;

    public void UpdateStep()
    {
        for(int i = 0; i < listBoss.Count; i++)
        {
            if (listBoss[i].gameObject.activeSelf)
            {
                listBoss[i].UpdateStep();
            }
        }
    }

    private void Update()
    {
        int myLevel = PlayerPrefs.GetInt("mylevel");
        if (myLevel < levelSpawnBoss    )
            return;

        if (Car.instance.isGameOver || Car.instance.isStartGame == false)
            return;

        if(timeGame < 5)
        {
            timeGame += Time.deltaTime;
        }
        else
        {
            if(isSpawnBoss == false)
            {
                isSpawnBoss = true;
                SpawnBoss();
            }
        }



    }

    private void SpawnBoss()
    {
        GameObject boss = GetObject();
        Vector3 temp = boss.transform.position;
        temp.x  = 0.0f;
        temp.y = 25;
        temp.z = -40;
        boss.transform.position = temp;

        boss.SetActive(true);
    }
}
