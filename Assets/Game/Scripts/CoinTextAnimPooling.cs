﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinTextAnimPooling : MonoBehaviour {

    public static CoinTextAnimPooling instance;

    private void Awake()
    {
        instance = this;
    }

    public GameObject prefab;
    private List<GameObject> listObject = new List<GameObject>();
    private const int count = 10;

    private void Start()
    {
        for (int i = 0; i < count; i++)
        {
            GenerateObject();
        }
    }

    private void GenerateObject()
    {
        GameObject objectClone = Instantiate(prefab, transform) as GameObject;
        objectClone.SetActive(false);
        listObject.Add(objectClone);
    }

    public GameObject GetObject()
    {
        for (int i = 0; i < listObject.Count; i++)
        {
            if (listObject[i].activeInHierarchy == false)
            {
                return listObject[i].gameObject;
            }
        }

        GameObject objectClone = Instantiate(prefab, transform) as GameObject;
        listObject.Add(objectClone);
        return objectClone;
    }
}
