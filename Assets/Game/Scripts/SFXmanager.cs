﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SFXmanager : MonoBehaviour {

	public static SFXmanager instance;
	public AudioClip coinClip;
	private AudioSource audioSource;

	void Awake()
	{
		instance = this;

	}
	void Start () {
		audioSource = GetComponent<AudioSource>();
	}
	
	// Update is called once per frame
	public void Coin_SFX(){
		audioSource.PlayOneShot(coinClip);
	}
}
