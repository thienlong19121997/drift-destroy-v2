﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Shop : MonoBehaviour {

    public static Shop instance;

    private void Awake()
    {
        instance = this;
    }

    public List<RectTransform> listObject = new List<RectTransform>();

    public int[] number;

    public int targetNumber;

    int count;
    Vector2 a, b, c ,d;
    Vector2 current;

    public GameObject parentInfo;
    public List<GameObject> listInfo = new List<GameObject>();
    public List<GameObject> btnBuys = new List<GameObject>();
    public List<GameObject> btnSelect = new List<GameObject>();
    public List<GameObject> btnSelected = new List<GameObject>();

    public Text coinText;

    private void Start()
    {
        if(PlayerPrefs.GetInt("playthefirsttime") == 0)
        {
            PlayerPrefs.SetInt("car0", 1);
          //  Car.instance.PlusCoin(50000);
            PlayerPrefs.SetInt("playthefirsttime", 1);
        }
        for (int i = 0; i < parentInfo.transform.childCount; i++)
        {
            listInfo.Add(parentInfo.transform.GetChild(i).gameObject);
        }

        for (int i = 0; i < listInfo.Count; i++)
        {
            btnBuys.Add(listInfo[i].transform.GetChild(2).gameObject);
            btnSelect.Add(listInfo[i].transform.GetChild(3).gameObject);
            btnSelected.Add(listInfo[i].transform.GetChild(4).gameObject);

        }

   

        count = transform.childCount;
        number = new int[count];
        a = new Vector2(-500, 0);
        b = new Vector2(0, 0);
        c = new Vector2(500, 0);
        d = new Vector2(-1000, 0);
        for (int i = 0; i < count; i++)
        {
            listObject.Add(transform.GetChild(i).gameObject.GetComponent<RectTransform>());
            number[i] = i;
            //if(i == 0)
            //{
            //    listObject[i].anchoredPosition = b;
            //}else if ( i == count - 1)
            //{
            //    listObject[i].anchoredPosition = a;
            //}
            //else if (i == 1)
            //{
            //    listObject[i].anchoredPosition = c;
            //}
            //else
            //{
            //    listObject[i].anchoredPosition = d;
            //}
            if (i == PlayerPrefs.GetInt("mycar"))
            {
                listObject[i].anchoredPosition = b;
            
                listInfo[i].SetActive(true);
                    
            
          
            }
            else
            {
                listObject[i].anchoredPosition = d;
                listInfo[i].SetActive(false);

            }
        }

        for(int i = 0; i < PlayerPrefs.GetInt("mycar"); i++)
        {
            for (int j = 0; j < number.Length; j++)
            {
                number[j]++;
                if (number[j] >= count)
                    number[j] = 0;
            }
        }

        for(int i = 0; i < btnSelect.Count; i++)
        {
            int a = i;
            btnSelect[i].GetComponent<Button>().onClick.AddListener(delegate { BtnSelect(a); });
        }
    

        UpdateShop();
    }

    float t;
    bool isMoveLeft;

    private void Update()
    {
        if(t < 1)
        {
            t += Time.deltaTime*4f;
        }
        listObject[number[0]].anchoredPosition = Vector2.Lerp(current, b, t);

        if (isMoveLeft)
        {
            listObject[number[1]].anchoredPosition = Vector2.Lerp(b, a, t);
        }
        else
        {
            listObject[number[count-1]].anchoredPosition = Vector2.Lerp(b, c, t);
        }
    }

    public void LeftArrow()
    {
        if (t < 1)
            return;

        t = 0;
        isMoveLeft = true;

        for(int i = 0; i < number.Length; i++)
        {
            number[i]--;
            if (number[i] < 0)
                number[i] = count - 1;
                    
        }


        listObject[number[0]].anchoredPosition = c;
        current = c;

        targetNumber = number[0];

        for(int i = 0; i< listInfo.Count; i++)
        {
            if(i == number[0])
            {
                listInfo[i].SetActive(true);
            }
            else
            {
                listInfo[i].SetActive(false);
            }
        }
    }

    public void RightArrow()
    {
        if (t < 1)
            return;

        t = 0;
        isMoveLeft = false;
        for (int i = 0; i < number.Length; i++)
        {
            number[i]++;
            if (number[i] >= count)
                number[i] = 0;
        }

        listObject[number[0]].anchoredPosition = a;
        current = a;

        targetNumber = number[0];
        targetNumber = number[0];

        for (int i = 0; i < listInfo.Count; i++)
        {
            if (i == number[0])
            {
                listInfo[i].SetActive(true);
            }
            else
            {
                listInfo[i].SetActive(false);
            }
        }
    }

    public void BtnBuy(int price)
    {
        int myCoin = PlayerPrefs.GetInt("coin");
        if(myCoin >= price)
        {
            myCoin -= price;
            coinText.text = myCoin.ToString();
            PlayerPrefs.SetInt("coin", myCoin);

            PlayerPrefs.SetInt("car" + targetNumber, 1);
            
        }
        UpdateShop();

        BtnSelect(targetNumber);
    }

    private void UpdateShop()
    {
        for(int i = 0; i < listInfo.Count; i++)
        {
            if(PlayerPrefs.GetInt("car"+i) >= 1)
            {
                btnBuys[i].SetActive(false);
                btnSelect[i].SetActive(true);
              //  btnSelected[i].SetActive(true);
                BtnSelect(PlayerPrefs.GetInt("mycar"));

            }
            else
            {
                btnBuys[i].SetActive(true);
                btnSelect[i].SetActive(false);
                btnSelected[i].SetActive(false);
            }
        }
    }

    public void BtnSelect(int number)
    {
        for(int i = 0;i  < btnSelect.Count; i++)
        {
            if(i == number)
            {
                btnSelect[i].SetActive(false);
              //  btnSelected[i].SetActive(true);
            }
            else
            {
                if (PlayerPrefs.GetInt("car" + i) >= 1)
                {
                    btnSelect[i].SetActive(true);
                    btnSelected[i].SetActive(false);
                }

            }
        }

        PlayerPrefs.SetInt("mycar", number);
        Car.instance.PickCar(PlayerPrefs.GetInt("mycar"));
    }

 
    

    

}
