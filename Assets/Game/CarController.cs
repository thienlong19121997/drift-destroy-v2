﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using UnityEngine.UI;

    /// <summary>
    /// Car controller class.
    /// Manages inputs from InputManager and driving evaluation
    /// You can customize the car properties for different types of cars
    /// </summary>
    public class CarController : MonoBehaviour
    {

        [Header("Engine")]
        [Range(1, 100)]
        /// the car's maximum speed
        public float MaximumVelocity = 20f;
        [Range(20, 100)]
        /// the engine's force
        public float Acceleration = 100f;
        [Range(1, 100)]
        /// the intensity of the brakes
        public float BrakeForce = 10f;
        [Range(1, 100)]
        /// the penalty applied when going offroad
        public float OffroadFactor = 2f; // penalty when going offroad

        [Header("Parameters")]
        // Point of gravity of the car is set below. This helps the Unity Physics with car stability
        public Vector3 CenterOfMass = new Vector3(0, -1, 0);

        // the maximal distance to the ground for IsGrounded evaluation
        public float CarGroundDistance = 0.1f;

        /// Gears enum. Car can be forward driving or backward driving (reverse)
        public enum Gears { forward, reverse }

        /// Current car velocity
        protected Vector3 _currentVelocity;

        /// The current gear value
        public Gears CurrentGear { get; protected set; }

        // Const used by the car engine
        // Feel free to edit these values. Just be sure to test thoroughly the new car driving behaviour
        protected const float _smallValue = 0.01f; // The minimal speed value at which car is changing from braking to going reverse
        protected const float _minimalGasPedalValue = 20f; // Minimal velocity value. Going less will means car can't move offroad and others glitches
        protected float _distanceToTheGroundRaycastLength = 50f; // raycast length used for Ground evaluation

        protected Vector3 _gravity = new Vector3(0, -30, 0); // Gravity is this world is set to -30 on the y axis


        /// <summary>
        /// Gets or sets the distance to the ground.
        /// </summary>
        /// <value>The distance to the ground.</value>
        protected virtual float DistanceToTheGround { get; set; }

        /// <summary>
        /// Gets or sets the ground game object.
        /// </summary>
        /// <value>The ground game object.</value>
        protected virtual GameObject GroundGameObject { get; set; }

        /// <summary>
        /// Gets a value indicating whether this car is off road.
        /// </summary>
        /// <value><c>true</c> if this instance is off road; otherwise, <c>false</c>.</value>

        public TrailRenderer[] trails_drift_normal;
        public TrailRenderer[] trails_drift_fever;



        public virtual bool IsOffRoad
        {
            get
            {
                return (GroundGameObject != null && GroundGameObject.tag == "OffRoad");
            }
        }
float Speed{
   get{ return _rigidbody.velocity.magnitude;}
}
        /// <summary>
        /// Gets the normalized speed.
        /// </summary>
        /// <value>The normalized speed.</value>
        public virtual float NormalizedSpeed {
            get
            {
                return Mathf.InverseLerp(0f, MaximumVelocity, Mathf.Abs(Speed));
            }
        }

        /// <summary>
        /// Physics initialization
        /// </summary>
        /// pub
        /// 

        public static CarController instance;

private Rigidbody _rigidbody;
          void Awake()
        {
            _rigidbody = GetComponent<Rigidbody>();
           // instance = this;


            // we set the physics gravity adapted to this game
            Physics.gravity = _gravity;

            // we change the center of mass below the vehicle to help with unity physics stability
            _rigidbody.centerOfMass += CenterOfMass;
        }


        protected virtual void Update()
        {
            _currentVelocity = _rigidbody.velocity;

            UpdateGroundSituation();

        }
        public Text driftText;

        float td;

        protected virtual void UpdateGroundSituation()
        {
            // // we cast a ray below the car to check if we're above ground and to determine the distance
            // RaycastHit raycast3D = MMDebug.Raycast3D(
            //     _collider.bounds.center,
            //     Vector3.down,
            //     _distanceToTheGroundRaycastLength,
            //     1 << LayerMask.NameToLayer("Ground"),
            //     Color.green,
            //     true);

            // if (raycast3D.transform != null)
            // {
            //     // We have a ground object
            //     DistanceToTheGround = raycast3D.distance;

            //     if (raycast3D.transform.gameObject != null)
            //     {
            //         // We store the ground object. (Will be used for offroad check)
            //         GroundGameObject = raycast3D.transform.gameObject;
            //     }
            // }

            // // Distance to the ground should be at a minimum
            // if ((DistanceToTheGround - _collider.bounds.extents.y) < CarGroundDistance)
            // {
            //     IsGrounded = true;
            // }
            // else
            // {
            //     IsGrounded = false;
            // }
        }

        public void SetTrail_Drift(bool isActive)
        {
            if (isActive == false)
            {
                for (int i = 0; i < trails_drift_fever.Length; i++)
                {

                    trails_drift_fever[i].emitting = isActive;

                }

                for (int i = 0; i < trails_drift_normal.Length; i++)
                {

                    trails_drift_normal[i].emitting = isActive;

                }
            }
            else
            {
                if (Car.instance.isFever)
                {
                    for (int i = 0; i < trails_drift_fever.Length; i++)
                    {

                        trails_drift_fever[i].emitting = isActive;

                    }

                    for (int i = 0; i < trails_drift_normal.Length; i++)
                    {

                        trails_drift_normal[i].emitting = !isActive;

                    }
                }
                else
                {
                    for (int i = 0; i < trails_drift_normal.Length; i++)
                    {

                        trails_drift_normal[i].emitting = isActive;

                    }

                    for (int i = 0; i < trails_drift_fever.Length; i++)
                    {

                        trails_drift_fever[i].emitting = !isActive;

                    }
                }
            }

        }


        public float currentGasPedalAmount;


        bool is_A;
        public void InputGetKeyDown_A()
        {
            is_A = true;
        }

        public void InputGetKeyUp_A()
        {
            is_A = false;


        }

        bool is_D;
        public void InputGetKeyDown_D()
        {
            is_D = true;
        }

        public void InputGetKeyUp_D()
        {
            is_D = false;


        }

        bool is_W = true;
        bool is_S;
        public bool returnTest;
        protected virtual void FixedUpdate()
        {
            if (returnTest)
                return;

            if (Car.instance.isStartGame == false || Car.instance.isGameOver || Car.instance.isGrounded == false)
                return;

        
            if (is_S)
            {
                if (currentGasPedalAmount > - .5f)
                    currentGasPedalAmount -= Time.deltaTime * 2;
            }
            else if (is_W)
            {
                CurrentGear = Gears.forward;
                if (currentGasPedalAmount < 1)
                    currentGasPedalAmount += Time.deltaTime * 2;
                Accelerate();
            }
            else
            {
                if(currentGasPedalAmount > 0)
                {
                    currentGasPedalAmount -= Time.deltaTime *2;
                }else if (currentGasPedalAmount < 0)
                {
                    currentGasPedalAmount += Time.deltaTime *2;
                }
            }

            if(is_A || is_D)
            {
                Car.instance.timeHoldDrift += Time.deltaTime;
                if(td < 1)
                td += Time.deltaTime;
            }
            else
            {
                if(td > 0)
                {
                    td -= Time.deltaTime;
                }
            }

            Color d = driftText.color;
            d.a = td;
            driftText.color = d;

            if (is_A)
            {
                directionWheel = -1;
                if (currentSteeringAmount > -1)
                {
                    currentSteeringAmount -= Time.deltaTime * 2;
                }      
            }
            else if (is_D)
            {
                directionWheel = 1;
                if (currentSteeringAmount < 1)
                {
                    currentSteeringAmount += Time.deltaTime * 2;
                }
            }
            else
            {
                if(currentSteeringAmount < 0.05f || currentSteeringAmount > -0.05f)
                {
                    currentSteeringAmount = 0;
                }
                if (currentSteeringAmount > 0)
                {
                    if (currentSteeringAmount > .2f)
                        currentSteeringAmount = .2f;
                    currentSteeringAmount -= Time.deltaTime;
                }
                else if (currentSteeringAmount < 0)
                {
                    if (currentSteeringAmount < -.2f)
                        currentSteeringAmount = -.2f;
                    currentSteeringAmount += Time.deltaTime;
                }
                else
                {
                    Car.instance.timeHoldDrift = 0;

                    if (isAutoRotate)
                    AutoRotate();
                }
                Vector2 dir = new Vector2(_rigidbody.velocity.x, _rigidbody.velocity.z);
                float an = Mathf.Atan2(dir.x, dir.y) * Mathf.Rad2Deg;
                var newRotation = Quaternion.Euler(0, an, 0);
                transform.rotation = Quaternion.Lerp(transform.rotation, newRotation, 1f * Time.deltaTime);

                if(wheelSpeed > 0)
                wheelSpeed -= Time.deltaTime;
            }


            if (currentSteeringAmount != 0)
            {
                Steer();
            }

      

            if (currentGasPedalAmount != 0)
            {
                // if it's fast enough, the car starts braking
                if ((Speed > _smallValue) && (CurrentGear == Gears.forward))
                {
                    Brake();
                }
                else
                {
                    // Otherwise, car is slow enough to go reverse
                    CurrentGear = Gears.reverse;
                    // Car is going reverse at half acceleration value
                    //currentGasPedalAmount /= 2;
                    Accelerate();
                }
            }

            if(is_A || is_D)
            {
                SetTrail_Drift(true);

            }
            else
            {
                SetTrail_Drift(false);

            }


            BossManager.instance.UpdateStep();
            Car.instance.CameraMainController();

        }

        public bool isAutoRotate;
        /// <summary>
        /// Manages the acceleration of the car (forward or reverse)
        /// </summary>
        /// 
        public float moveSpeed;
		protected virtual void Accelerate() 
		{
            if (_rigidbody.velocity.magnitude > MaximumVelocity) 
			{
				// if the car goes fast enough, we don't do anything and exit
				return;
			}

			// If we are offroad, we penalize acceleration and max velocity
			if ( GroundGameObject != null && GroundGameObject.tag == "OffRoad") 
			{
				if (_rigidbody.velocity.magnitude > ( MaximumVelocity / OffroadFactor))
				{
					// if the car is going fast enough in an offroad ground, we don't do anything
					return;
				}

                // acceleration is divided by the offroad penalty factor
                currentGasPedalAmount /= OffroadFactor;
			}

			// the force of acceleration has a minimal value to avoid having the car blocked by drag / friction and material physics interaction
			float force = Mathf.Max(_minimalGasPedalValue, Mathf.Abs(currentGasPedalAmount * Acceleration));

			// if the acceleration is negative, we change the force value sign
			if (currentGasPedalAmount < 0) 
			{
				force = -force;
			}

            // we apply the new velocity
            float feverSpeed;
            if (Car.instance.isFever)
            {
                feverSpeed = 3.0f;
            }
            else
            {
                feverSpeed = 1.5f;
            }

            _rigidbody.velocity = _rigidbody.velocity + (transform.forward * force * Time.fixedDeltaTime * feverSpeed * Car.instance.timeSpeed );
     


        }

        /// <summary>
        /// Brake force
        /// </summary>
        protected virtual void Brake() 
		{
            float feverSpeed;
            if (Car.instance.isFever)
            {
                feverSpeed = 2.0f;
            }
            else
            {
                feverSpeed = 1.5f;
            }
            // only if the car is moving
            if ((Speed > 0) && (CurrentGear == Gears.forward)) 
			{
				// we apply the new velocity
				_rigidbody.velocity = _rigidbody.velocity - (transform.forward * BrakeForce * Time.fixedDeltaTime * feverSpeed );	        
			}
	    }


        private float currentSteeringAmount;

        float wheelSpeed;
        int directionWheel;

        protected virtual void Steer() 
		{
            //float steeringAmount = currentSteeringAmount * Time.fixedDeltaTime * SteeringSpeed;
            float steeringAmount = directionWheel * Time.fixedDeltaTime * 150 *1.1f;
            // Going backward, we invert steering
            if (CurrentGear == Gears.reverse) 
			{
				steeringAmount = -steeringAmount;
			}

            float feverSpeed;
            if (Car.instance.isFever)
            {
                feverSpeed = 1.5f;
            }
            else
            {
                feverSpeed = 1.25f;
            }

            transform.Rotate(steeringAmount * NormalizedSpeed * Vector3.up * feverSpeed );
            float d = steeringAmount * NormalizedSpeed * feverSpeed;
            Car.instance.PlusDriftValue(Mathf.Abs(d));
            //  Debug.Log(steeringAmount + " __ ");


            // xoay banh xe 
            if (Car.instance.wheel_L == null)
                return;

            GameObject wheel_L = Car.instance.wheel_L;
            GameObject wheel_R = Car.instance.wheel_R;

            if (wheelSpeed < 1)
                wheelSpeed += Time.deltaTime;

            wheel_L.transform.Rotate(Vector3.up * directionWheel * wheelSpeed * 2);
            wheel_R.transform.Rotate(Vector3.up * directionWheel * wheelSpeed * 2);

            Vector3 temp = wheel_L.transform.eulerAngles;
            temp.y = Mathf.Clamp(wheel_L.transform.eulerAngles.y, -15, 15);
            wheel_L.transform.eulerAngles = wheel_R.transform.eulerAngles = temp;

        }

        private const float speedSmartRotate = 40.0f;

        void AutoRotate()
        {

            if (transform.eulerAngles.y <= 45 || transform.eulerAngles.y > 315)
            {
                Vector3 temp = transform.eulerAngles;

                if (transform.eulerAngles.y <= 45)
                {
                    if (temp.y > 2)
                    {
                        temp.y -= Time.deltaTime * speedSmartRotate;
                    }
                    else
                    {
                        temp.y = 0;
                    }
                }
                else
                {
                    if (temp.y < 358)
                    {
                        temp.y += Time.deltaTime * speedSmartRotate;
                    }
                    else
                    {
                        temp.y = 0;
                    }
                }

                transform.eulerAngles = temp;

            }
            else if (transform.eulerAngles.y <= 315 && transform.eulerAngles.y > 225)
            {
                Vector3 temp = transform.eulerAngles;

                if (temp.y > 272)
                {
                    temp.y -= Time.deltaTime * speedSmartRotate;
                }
                else if (temp.y < 268)
                {
                    temp.y += Time.deltaTime * speedSmartRotate;
                }
                else
                {
                    temp.y = 270;
                }

                transform.eulerAngles = temp;
            }
            else if (transform.eulerAngles.y <= 225 && transform.eulerAngles.y > 135)
            {
                Vector3 temp = transform.eulerAngles;

                if (temp.y > 182)
                {
                    temp.y -= Time.deltaTime * speedSmartRotate;
                }
                else if (temp.y < 178)
                {
                    temp.y += Time.deltaTime * speedSmartRotate;
                }
                else
                {
                    temp.y = 180;
                }

                transform.eulerAngles = temp;
            }
            else if (transform.eulerAngles.y <= 135 && transform.eulerAngles.y > 45)
            {
                Vector3 temp = transform.eulerAngles;

                if (temp.y > 92)
                {
                    temp.y -= Time.deltaTime * speedSmartRotate;
                }
                else if (temp.y < 88)
                {
                    temp.y += Time.deltaTime * speedSmartRotate;
                }
                else
                {
                    temp.y = 89;
                }

                transform.eulerAngles = temp;

            }


        }
	}
