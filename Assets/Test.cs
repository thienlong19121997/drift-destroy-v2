﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
public class Test : MonoBehaviour {

    public GameObject target;
    public float moveSpeed;
    public float rotationSpeed;
    private Vector3 smoothVelocity = Vector3.zero;
    private void Update()
    {
       // transform.LookAt(target.transform, Vector3.forward);
        transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(target.transform.position - transform.position), rotationSpeed * Time.deltaTime);
        transform.Translate(Vector3.forward * moveSpeed * Time.deltaTime);
        }

}
